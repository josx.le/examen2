from django import forms
from .models import *

##STADIUM

class Stadiumform(forms.ModelForm):
    class Meta:
        model = Stadium
        fields = "__all__"
        exclude = ["timestamp", "final_date"]
        widgets = {
            "user": forms.Select(attrs={"type":"select","class":"form-select"}),
            "gg_title": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del GrantGoal"}),
            "description": forms.Textarea(attrs={"type":"text", "class":"form-control", "rows":3, "placeholder":"Escribe la descripcion del GrantGoal"}),
            }


class UpdateStadiumForm(forms.ModelForm):
    class Meta:
        model = Stadium
        fields = "__all__"
        exclude = ["timestamp", "final_date"]
        widgets = {
            "user": forms.Select(attrs={"type":"select","class":"form-select"}),
            "gg_title": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del GrantGoal"}),
            "description": forms.Textarea(attrs={"type":"text", "class":"form-control", "rows":3, "placeholder":"Escribe la descripcion del GrantGoal"}),
            }

##TEAM

class Teamform(forms.ModelForm):
    class Meta:
        model = Team
        fields = "__all__"
        widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Whrite name of the team"}),
            "description": forms.Textarea(attrs={"type":"text", "class":"form-control", "rows":3, "placeholder":"whhrite the description"}),
            }
        
class UpdateTeamform(forms.ModelForm):
    class Meta:
        model = Team
        fields = "__all__"
        widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Whrite name of the team"}),
            "description": forms.Textarea(attrs={"type":"text", "class":"form-control", "rows":3, "placeholder":"whhrite the description"}),
            }

##CITY

class Cityform(forms.ModelForm):
    class Meta:
        model = City
        fields = "__all__"
        widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Whrite name of the City"}),
            }

class UpdateCityform(forms.ModelForm):
    class Meta:
        model = City
        fields = "__all__"
        widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Whrite name of the City"}),
            }