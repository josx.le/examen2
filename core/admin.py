from django.contrib import admin

# Register your models here.

from .models import *

@admin.register(Stadium)
class StadiumAdmin(admin.ModelAdmin):
    list_display = [
        "gg_title",
        "user",
        "desciption",
    ]

@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = [
        "name1",
        "desciption2",
    ]

@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = [
        "name2",
    ]
